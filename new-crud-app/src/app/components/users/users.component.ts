import { Component, OnInit } from '@angular/core';
import {User} from "../../models/User";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[];
  currentUser: User = {
    id: 0,
    name: '',
    username: '',
    email: '',
    address: {
      street: '',
      suite: '',
      city: '',
      zipcode: '',
      geo: {
        lat: '',
        lng: ''
      }
    },
    phone: '',
    website: '',
    company: {
      name: '',
      catchPhrase: '',
      bs: ''
    }

  };


  constructor(private dataService: DataService) { }

  ngOnInit(): void {

    this.dataService.getUsers().subscribe(user => {
      console.log(user);
      this.users = user;
    })
  }

  onNewUser(user : User) {
    this.users.unshift(user);
  }

}
