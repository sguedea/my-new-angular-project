import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../models/User";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Output() newUser: EventEmitter<User> = new EventEmitter();
  @Output() updatedUser: EventEmitter<User> = new EventEmitter();

  @Input() currentUser: User;
  @Input() isEdit: boolean;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  addUser(name) {
    if (!name) {
      alert('Please enter a name')
    } else {
      this.dataService.saveUser({name} as User).subscribe( user => {
        this.newUser.emit(user)
      })
    }
  }

}
